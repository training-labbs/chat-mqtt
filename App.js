import React from 'react';
import { StyleSheet, Text, View, Alert, TextInput, Button, AsyncStorage } from 'react-native';
import init from 'react_native_mqtt';

init({
  size: 10000,
  storageBackend: AsyncStorage,
  defaultExpires: 1000 * 3600 * 24,
  enableCache: true,
  reconnect: true,
  sync : {
  }
});
const client = new Paho.MQTT.Client('echo.websocket.org', 443, 'uname');


export default class App extends React.Component {


  constructor(props) {
    super(props);
    this.state = { text: '' , chat: [], apelido: ''};
  }


  

    _inicializarMQTT = () => {
  
       

      onConnect = () => {
        console.log("onConnect");
        client.subscribe("chatLabbs");
      }
      client.onConnectionLost = this.onConnectionLost;
      client.onMessageArrived = this.onMessageArrived;
      client.onMessageDelivered = this.onMessageDelivered;
      client.connect({ onSuccess:onConnect, useSSL: false });

    };
    
    

  componentDidMount(){

    this._inicializarMQTT();
    
  }

  onConnectionLost = (responseObject) => {
    if (responseObject.errorCode !== 0) {
      console.log("onConnectionLost:"+responseObject.errorMessage);
    }
  }
   
  onMessageArrived = (message) => {
    console.log("onMessageArrived:"+message.payloadString);
    let conversa = this.state.chat;
    conversa.push(message.payloadString + '\n');
    this.setState({ chat: conversa })
  }

  onMessageDelivered = (message) => {
    console.log("onMessageDelivered:"+message.payloadString);
  }

  _clicouEnviar = () =>{
    let mensagemFormatada = this.state.apelido + ': ' + this.state.text;
    message = new Paho.MQTT.Message(mensagemFormatada);

    message.destinationName = "chatLabbs";
    client.send(message);
  
  }

  render() {
    {{let textoChat = '';
      console.log("Renderizei!!");}}
    return (
      <View style={styles.container}>
        <Text>Apelido:</Text>
        <TextInput
          style={styles.inputApelido}
          onChangeText={(apelido) => this.setState({apelido})}
          value={this.state.apelido}
        />
        <TextInput
          style={styles.inputMain}
          onChangeText={(mario) => this.setState({ text: mario})}
          value={this.state.text}
        />
        <Button
          title="Enviar"
          onPress={this._clicouEnviar}>
        </Button>
        <Text style={styles.caixaTexto}>{this.state.chat}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
  inputMain: {
    height: 40,
    width: 180,
    borderColor: 'gray',
    borderWidth: 1
  },
  caixaTexto: {
    borderWidth: 1,
    width: 300,
    height: 200
  },
  inputApelido: {
    height: 40,
    width: 100,
    borderColor: 'gray',
    borderWidth: 1
  }
});
